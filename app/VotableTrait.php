<?php

namespace App;

trait VotableTrait {

    public function downVotes() {
        return $this->votes()->wherePivot('vote', -1)->sum('vote');
    }

    public function upVotes() {
        return $this->votes()->wherePivot('vote', 1)->sum('vote');
    }

    public function votes() {
        return $this->morphToMany(User::class, 'votable');
    }
}
