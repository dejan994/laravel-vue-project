<?php

namespace App\Http\Controllers;

use App\Answer;

class AcceptAnswerController extends Controller
{
    /**
     * @param Answer $answer
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function __invoke(Answer $answer)
    {
        $this->authorize('accept', $answer);

        $answer->question->acceptBestAnswer($answer);

        if(request()->expectsJson()) {
            return response()->json([
                'message' => 'Answer accepted as best answer'
            ]);
        }

        return back();
    }
}
