@csrf
<div class="form-group">
    <label for="question-title">Question Title:</label>
    <input type="text" name="title" value="{{ old('title', $question->title) }}" id="question-title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" placeholder="Enter title">

    @if ($errors->has('title'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('title') }}</strong>
        </div>
    @endif
</div>
<div class="form-group">
    <label for="question-body">Explain your question:</label>
    <textarea name="body" id="question-body" rows="10"  class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}" placeholder="Explain your question">{{ old('body', $question->body) }}</textarea>

    @if ($errors->has('body'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('body') }}</strong>
        </div>
    @endif
</div>
<button type="submit" class="btn btn-primary">{{ $buttonText }}</button>
