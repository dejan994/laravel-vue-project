@if ($model instanceof App\Question)
    @php
        $name = 'question';
        $firstURISegment = 'questions';
    @endphp
@elseif ($model instanceof App\Answer)
    @php
        $name = 'answer';
        $firstURISegment = 'answers';
    @endphp
@endif

<div class="d-flex flex-column vote-controls">
    <a title="This {{ $name }} is useful" class="vote-up"
       {{ \Illuminate\Support\Facades\Auth::guest() ? 'off' : '' }}
       onclick="event.preventDefault(); document.getElementById('up-vote-{{ $name }}-{{ $model->id }}').submit();"
    >
        <i class="fas fa-caret-up fa-2x"></i>
    </a>
    <form class="hidden-form-vote-up-{{ $name }}" id="up-vote-{{ $name }}-{{ $model->id }}" action="/{{ $firstURISegment }}/{{ $model->id }}/vote" method="POST">
        @csrf
        <input type="hidden" name="vote" value="1">
    </form>
    <span class="votes-count">{{$model->votes_count}}</span>
    <a title="This {{ $name }} is not useful" class="vote-down"
       {{ \Illuminate\Support\Facades\Auth::guest() ? 'off' : '' }}
       onclick="event.preventDefault(); document.getElementById('down-vote-{{ $name }}-{{ $model->id }}').submit();">
        <i class="fas fa-caret-down fa-2x"></i>
    </a>
    <form class="hidden-form-vote-up-{{ $name }}" id="down-vote-{{ $name }}-{{ $model->id }}" action="/{{ $firstURISegment }}/{{ $model->id }}/vote" method="POST">
        @csrf
        <input type="hidden" name="vote" value="-1">
    </form>

    @if($model instanceof App\Question)
        <favorite :question="{{ $model }}"></favorite>
    @elseif($model instanceof App\Answer)
        <accept :answer="{{ $model }}"></accept>
    @endif
</div>
