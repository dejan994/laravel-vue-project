<a title="Click to mark as favorite(click again to undo)"
   class="favorite mt-2 {{ \Illuminate\Support\Facades\Auth::guest() ? 'off' : ($model->is_favorited ? 'favorited' : '') }}"
   onclick="event.preventDefault(); document.getElementById('favorite-{{ $name }}-{{ $model->id }}').submit();">
    <i class="fas fa-star fa-2x"></i>
    <span class="favorites-count">{{ $model->favorites_count }}</span>
</a>
<form class="hidden-form-favorites" id="favorite-{{ $name }}-{{ $model->id }}" action="/{{ $name }}s/{{ $model->id }}/favorites" method="POST">
    @csrf
    @if($model->is_favorited)
        @method ('DELETE')
    @endif
</form>
